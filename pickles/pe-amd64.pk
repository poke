/* pe-amd64.pk -- PE COFF implementation for GNU poke, AMD64.  */

/* Copyright (C) 2022, 2023, 2024, 2025 Jose E. Marchesi.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* AMD 64 relocations.  */

var PE_AMD64_R_ABOLUTE = 0x0000UH,
    PE_AMD64_R_ADDR64 = 0x0001UH,
    PE_AMD64_R_ADDR32 = 0x0002UH,
    PE_AMD64_R_ADDR32NB = 0x0003UH,
    PE_AMD64_R_REL32 = 0x0004UH,
    PE_AMD64_R_REL32_1 = 0x0005UH,
    PE_AMD64_R_REL32_2 = 0x0006UH,
    PE_AMD64_R_REL32_3 = 0x0007UH,
    PE_AMD64_R_REL32_4 = 0x0008UH,
    PE_AMD64_R_REL32_5 = 0x0009UH,
    PE_AMD64_R_SECTION = 0x000AUH,
    PE_AMD64_R_SECREL = 0x000BUH,
    PE_AMD64_R_SECREL7 = 0x000CUH,
    PE_AMD64_R_TOKEN = 0x000DUH,
    PE_AMD64_R_SREL32 = 0x000EUH,
    PE_AMD64_R_PAIR = 0x000FUH,
    PE_AMD64_R_SSPAN32 = 0x0010UH;
