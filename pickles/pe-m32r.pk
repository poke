/* pe-m32r.pk -- PE COFF implementation for GNU poke, M32R.  */

/* Copyright (C) 2022, 2023, 2024, 2025 Jose E. Marchesi.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Mitsubishi M32R relocations.  */

var PE_M32R_R_ABSOLUTE = 0x0000UH,
    PE_M32R_R_ADDR32 = 0x0001UH,
    PE_M32R_R_ADDR32NB = 0x0002UH,
    PE_M32R_R_ADDR24 = 0x0003UH,
    PE_M32R_R_GPREL16 = 0x0004UH,
    PE_M32R_R_PCREL24 = 0x0005UH,
    PE_M32R_R_PCREL16 = 0x0006UH,
    PE_M32R_R_PCREL8 = 0x0007UH,
    PE_M32R_R_REFHALF = 0x0008UH,
    PE_M32R_R_REFHI = 0x0009UH,
    PE_M32R_R_REFLO = 0x000AUH,
    PE_M32R_R_PAIR = 0x000BUH,
    PE_M32R_R_SECTION = 0x000CUH,
    PE_M32R_R_SECREL = 0x000DUH,
    PE_M32R_R_TOKEN = 0x000EUH;
