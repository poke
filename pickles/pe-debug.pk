/* pe-debug.pk -- PE COFF implementation for GNU poke, debug info.  */

/* Copyright (C) 2022, 2023, 2024, 2025 Jose E. Marchesi.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Frame pointer omission (FPO) info.
   This is the raw data of debug entries of type PE_DEBUG_TYPE_FPO.  */

var PE_DEBUG_FRAME_FPO = 0,
    PE_DEBUG_FRAME_TRAP = 1,
    PE_DEBUG_FRAME_TSS = 2;

type PE_FPO_Data =
  struct
  {
    offset<uint<32>,B> proc_offset;
    offset<uint<32>,B> proc_size;
    /* Size of locals and params, / 4 */
    offset<uint<16>,B> locals;
    offset<uint<16>,B> params;
    struct uint<16>
      {
        /* Size of function prologue.  */
        offset<uint<8>,B> prolog_size;
        uint<3> num_regs_saved;
        uint<1> has_seh;
        uint<1> use_ebp;
        uint<1>;
        uint<2> frame_type;
      } info;
  };

/* Hash for reproducible PE files.
   This is the raw data of debug entries of type PE_DEBUG_TYPE_REPRO.  */

type PE_Repro_Hash =
  struct
  {
    offset<uint<32>,B> size;
    byte[size] value;
  };

/* Extended DLL characteristics/flags bits.

   These are used in raw debug data of type
   PE_DEBUG_TYPE_EX_DLLCHARACTERISTICS.  */

var PE_DLL_F_EX_CET_COMPAT = 0xo0001UH;
